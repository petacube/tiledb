
for D in ./*; do
    if [ -d "$D" ]; then
        cd "$D"
        rm -fr .git
        cd ..
    fi
done