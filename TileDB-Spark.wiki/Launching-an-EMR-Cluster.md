This guide will walk through the steps needed to launch an EMR cluster with TileDB-Spark bootstrapped.

# How to use the bootstrap scripts

Under [scripts/emr_bootstrap](scripts/emr_bootstrap) there are two handy scripts for setting up TileDB-Spark and Apache arrow. Apache arrow is optional but will increase performance if using [pyspark](https://spark.apache.org/docs/latest/sql-pyspark-pandas-with-arrow.html) or [sparkR](https://github.com/apache/spark/blob/master/docs/sparkr.md#apache-arrow-in-sparkr). We found there was a lack of bootstrap instructions for arrow so we created a simple script if you desire to use arrow.

## Prerequisites

### Copying Scripts to S3 Bucket

First you must clone this repo (to get the bootstrap scripts).

```
git clone https://github.com/TileDB-Inc/TileDB-Spark.git
cd TileDB-Spark
```


EMR requires that the bootstrap script be copied to an s3 bucket. Below is a sample command of how to sync the scripts from this repo to s3, please replace the s3 bucket with your own.

```
aws s3 sync scripts/emr_bootstrap s3://my_bucket/path/emr_bootstrap
```

## Launching EMR Cluster

## Configuring Metrics

TileDB-Spark provide a metric source to collect timing and input metric details This can be helpful in tracking performance of TileDB and the TileDB-Spark driver. Enabling metrics is optional.


On step 1 of the EMR launch cluster console there is a section for `Edit software settings`. There you can paste the following json config which will enable the spark metrics source from TileDB.

```
[{"classification":"spark-metrics","properties":{"driver.source.io.tiledb.spark.class":"org.apache.spark.metrics.TileDBMetricsSource","executor.source.io.tiledb.spark.class":"org.apache.spark.metrics.TileDBMetricsSource"}},{"classification":"spark-log4j","properties":{"log4j.logger.io.tiledb.spark":"INFO"}}]
```

## Setting Bootstrap Actions in EMR Console

Below are the steps to configure the bootstrap for TileDB.

1. Using the AWS EMR Console create a cluster and choose *advanced options*.
2. In *Step 3* you can configure your *bootstraps*. Choose to *Configure and add* a *Custom action* to add a second script to install TileDB-Spark
	* For the *Name* you can fill something like _Install TileDB-Spark_
	* For the *Script location* you will need to point to where you have uploaded the *gist* (Eg. `s3://my_bucket/path/emr_bootstrap/install-tiledb-spark.sh`)


### Arrow Boostrap

If you also would like to bootstrap apache arrow please use the following two additional bootstrap options

1. In *Step 3* you can configure your *bootstraps*. Choose to *Configure and add* a *Custom action* to add a second script to install TileDB-Spark
	* For the *Name* you can fill something like _Install Arrow_
	* For the *Script location* you will need to point to where you have uploaded the *gist* (Eg. `s3://my_bucket/path/emr_bootstrap/install-apache-arrow.sh`)
2. In *Step 3* you can configure your *bootstraps*. Choose to *Configure and add* a *Custom action*
	* For the *Name* you can fill something like _Install CRAN dependencies_
	* For the *Script location* you will need to point to where you have uploaded the *gist* (Eg. `s3://my_bucket/path/emr_bootstrap/install-cran-packages.sh`)
	* As *Optional arguments* you must add the following:
		* `--packages` - Where you list all of the *CRAN* packages that you depend on, separated by semicolon. Eg: `--packages arrow;zoo` . At minimum `arrow` is needed if you want to use sparkR with arrow support.


## Launching the Cluster

After the bootstrap actions are configured you can proceed to launch the cluster. The bootstrapping can take 10-20 minutes depending but once it is done you will have a spark cluster that supports TileDB.

## Credits

* [https://gist.github.com/cosmincatalin/866233457e28ecb9224b126cd2747cb5](https://gist.github.com/cosmincatalin/866233457e28ecb9224b126cd2747cb5)
