
## Books

Spark SQL Gitbook:
[https://jaceklaskowski.gitbooks.io/mastering-spark-sql/](https://jaceklaskowski.gitbooks.io/mastering-spark-sql/)

## Videos

RDDs, Dataframes, and Datasets in Apache Spark:
[https://www.youtube.com/watch?v=pZQsDloGB4w](https://www.youtube.com/watch?v=pZQsDloGB4w)