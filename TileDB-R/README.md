<a href="https://tiledb.com"><img src="https://github.com/TileDB-Inc/TileDB/raw/dev/doc/source/_static/tiledb-logo_color_no_margin_@4x.png" alt="TileDB logo" width="400"></a>

[![Build Status](https://travis-ci.org/TileDB-Inc/TileDB-R.svg?branch=master)](https://travis-ci.org/TileDB-Inc/TileDB-R)

# TileDB-R

`TileDB-R` is a [R](https://www.r-project.org/) interface to the [TileDB Storage Engine](https://github.com/TileDB-Inc/TileDB). 

**Warning**: The R interface to TileDB is under development and the API is subject to change.

## Quick Links

- [Installation](https://docs.tiledb.com/developer/installation)
- [Quickstart](https://docs.tiledb.com/developer/quickstart)
- [Reference Docs](https://tiledb-inc.github.io/TileDB-R/)


## Compatibility

TileDB-R follows semantic versioning. Currently tiledb core library does not,
as such the below table reference which versions are compatible.

| TileDB-R Version | TileDB Version |
| ----------------- | -------------- |
| 0.1.X             | 1.3.X          |
| 0.2.X             | 1.6.X          |
| 0.3.X             | 1.7.X          |

